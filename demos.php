<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "includes/head.php"; ?>
</head>

<body>

  <?php include "includes/nav.php"; ?>
    <div class="jumbotron">
      <div class="container">
        <h1>Demos &amp; Applications</h1>
      <p></p>
    </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-lg-6 col-md-6">
          <h2>HydroServer Lite</h2>
          <p>The HydroServer Lite Interactive Web Client is an online software tool that helps store, organize, and publish data.
HydroServer Lite allows users to collect and share scientific data with professional scientists to achieve common goals.
Once you are a registered user, you will be able to login and upload your own data into our database to provide valuable input into the research being done in your area as well as around the world.</p>
        </div>
        <div class="col-lg-5 col-md-5 col-md-offset-1 col-lg-offset-1">
          <a href="http://hsl.geology.isu.edu/client/"><img src="assets/images/HydroServer_thumbnail.jpg" class="center-block img-responsive"></a>
        </div>
      </div>

        <div class="row">
          <div class="col-lg-6 col-md-6">
            <h2>Understanding Climate Change through Historical Photography (in development)</h2>
            <p>How has this lake changed in the last few years? What did it look last year? How is the lake and environment responding to warmer, longer summers? These are all questions that can be answered, at least in part, by comparing photographs taken over time.</p>
            <p>This website houses the historical photography database for Lake Waiau, Hawaii. Our goal is to provide a searchable, interactive platform to explore, use, and contribute to this growing database.</p>
            <p>Browse photos of the lake or park, comment on the changes you see, learn about the monitoring informed by the photographs, and upload your own photos to the database. Also, let us know what you think of the site.</p>

          </div>
          <div class="col-lg-5 col-md-5 col-md-offset-1 col-lg-offset-1">
            <img src="assets/images/LakeWaiau_thumbnail.jpg" class="center-block img-responsive">
          </div>
        </div>

        <div class="row">
          <div class="col-lg-6 col-md-6">
            <h2>Structure from Motion (SfM)</h2>
            <p>Structure from Motion allows researchers to capture and record data from overlapping photographs. This image on the right is ISU's Bengal Tiger statue that was created from photos taken on a smartphone.</p>
          </div>
          <div class="col-lg-5 col-md-5 col-md-offset-1 col-lg-offset-1">
            <div class="embed-responsive embed-responsive-16by9">
              <iframe width="640" height="480" src="https://sketchfab.com/models/44e5a62f25004e5ba773d74be65ba601/embed" frameborder="0" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" onmousewheel="" class="embed-responsive-item" ></iframe>
            </div>
            </div>
          </div>
        </div>

    </div><!-- /.container -->

    <?php include "includes/footer.php"; ?>
    <?php include "includes/scripts.php"; ?>

  </body>
</html>
