<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "includes/head.php"; ?>
</head>

<body>

  <?php include "includes/nav.php"; ?>


<div class="jumbotron">
  <div class="container">
    <h1>Research</h1>
    <p></p>
  </div>
</div>
    <div class="container content">
      <div class="row">
        <div class="col-lg-6 col-md-6">
          <h2>Unmanned Aircraft Systems (UAS)</h2>
          <p>Unmanned aircraft systems allow for high resolution remote sensing for various applications. We are experimenting with DSLR, infrared and multi-spectral cameras for data capture. From these images we can create 3D models and conduct vegetation analysis. These systems have high potential for precision agriculture, archaeology, geology and land use change.</p>
        </div>
        <div class="col-lg-5 col-md-5 col-md-offset-1 col-lg-offset-1">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe src="https://player.vimeo.com/video/79628271" class="embed-responsive-item" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-6 col-md-6">
          <h2>Snow Avalanches and Mass Movements</h2>
          <p>Simulating snow avalanche flow with numerical models and digital elevation model (DEM) data has allowed snow scientists to better understand flow movement in complex terrain. In collaboration with a team of researchers, we are building a prototype from gaming technology to simulate avalanche flow with an OpenGL particle physics based emulation. We are planning to adapt this model for other types of mass movements including rockfall and landslides.</p>
        </div>
        <div class="col-lg-5 col-md-5 col-md-offset-1 col-lg-offset-1">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe src="https://player.vimeo.com/video/68069006" class="embed-responsive-item" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-6 col-md-6">
          <h2>Landscape Change</h2>
          <p>Examining how vegetation, urban areas and terrain vary over time provides insight into how anthropogenic/cultural/historical factors have changed our landscape. For this particular research project we are examining the vulnerability of cultural/historical resources to current invasive vegetation distribution and predicted vegetation extent based upon climate change dynamics.</p>
        </div>
        <div class="col-lg-5 col-md-5 col-md-offset-1 col-lg-offset-1">
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
              <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
              <div class="item active">
                <img src="assets/images/LidarSlider1.jpg" alt="Chania" class="center-block img-responsive">
              </div>

              <div class="item">
                <img src="assets/images/LidarSlider2.jpg" alt="Chania" class="center-block img-responsive">
              </div>

              <div class="item">
                <img src="assets/images/LidarSlider3.jpg" alt="Flower" class="center-block img-responsive">
              </div>

              <div class="item">
                <img src="assets/images/LidarSlider4.jpg" alt="Flower" class="center-block img-responsive">
              </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-6 col-md-6">
          <h2>Landscape Change</h2>
          <p>Examining how vegetation, urban areas and terrain vary over time provides insight into how anthropogenic/cultural/historical factors have changed our landscape. For this particular research project we are examining the vulnerability of cultural/historical resources to current invasive vegetation distribution and predicted vegetation extent based upon climate change dynamics.</p>
        </div>
        <div class="col-lg-5 col-md-5 col-md-offset-1 col-lg-offset-1">
          <div class="embed-responsive embed-responsive-16by9">
            <img src="assets/images/landscapechange2.jpg" class="center-block img-responsive">
          </div>
        </div>
      </div>


    </div><!-- /.container -->
    
    <?php include "includes/footer.php"; ?>
    <?php include "includes/scripts.php"; ?>
  </body>
</html>
