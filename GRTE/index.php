<!DOCTYPE html>
<html lang="en">

    <head>
	<?php include "includes/head.php"; ?>
    </head>
    <!-- NAVBAR
	 ================================================== -->

    <body>
	<?php include "includes/nav.php"; ?>


	<!-- Carousel
	     ================================================== -->
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
		<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		<li data-target="#myCarousel" data-slide-to="1"></li>
		<li data-target="#myCarousel" data-slide-to="2"></li>
		<li data-target="#myCarousel" data-slide-to="3"></li>
		<li data-target="#myCarousel" data-slide-to="4"></li>
		<li data-target="#myCarousel" data-slide-to="5"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
		<div class="item active">
                    <img class="first-slide img-responsive" src="../assets/images/grte/pouch_banner_min.jpg" alt="First slide">
                    <div class="container">
			<div class="carousel-caption">
                            <h3>Grand Teton National Park</h3>
                            <p>3D Models</p>
			</div>
                    </div>
		</div>
		<div class="item">
                    <img class="second-slide img-responsive" src="../assets/images/grte/basket_banner_min.jpg" alt="Second slide">
                    <div class="container">
			<div class="carousel-caption">
                            <h3>Grand Teton National Park</h3>
                            <p>3D Models</p>
			</div>
                    </div>
		</div>
		<div class="item">
                    <img class="third-slide img-responsive" src="../assets/images/grte/gtnpvc.jpg" alt="Third slide">
                    <div class="container">
			<div class="carousel-caption">
                            <h3>Grand Teton National Park</h3>
                            <p>Visitor Center</p>
                            <p class="pull-right image-credit" ><a href="https://commons.wikimedia.org/wiki/File:Grand_Teton_NP_new_VC_GTNP1.jpg" target="_blank">Image: Wikimedia Commons</a></p>
			</div>
                    </div>
		</div>
		<div class="item">
                    <img class="second-slide img-responsive" src="../assets/images/grte/youngs_delparte.jpg" alt="Second slide">
                    <div class="container">
			<div class="carousel-caption">
                            <h3  class="carousel-caption-title">Youngs and Delparte discussing 3D image creation</h3>
			</div>
                    </div>
		</div>
		<div class="item">
                    <img class="third-slide img-responsive" src="../assets/images/grte/teton2.jpg" alt="Third slide">
                    <div class="container">
			<div class="carousel-caption">
                            <h3  class="carousel-caption-title">Youngs and Guild</h3>
			</div>
                    </div>
		</div>
		<div class="item">
                    <img class="forth-slide img-responsive" src="../assets/images/grte/teton4.jpg" alt="Forth slide">
                    <div class="container">
			<div class="carousel-caption">
                            <h3  class="carousel-caption-title">3D Image Creation</h3>
			</div>
                    </div>
		</div>
		<!--<div class="item">
		     <img class="third-slide" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Third slide">
		     <div class="container">
		     <div class="carousel-caption">
		     <h1>One more for good measure.</h1>
		     <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
		     <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
		     </div>
		     </div>
		     </div>-->
            </div>
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
            </a>
	</div>
	<!-- /.carousel -->


	<!-- Marketing messaging and featurettes
	     ================================================== -->
	<!-- Wrap the rest of the page in another container to center all the content. -->

	<a id="gallery"></a>

	<div class="container-fluid marketing">
            <!-- Three columns of text below the carousel -->

	    <div class="bar-header"><em>The Vernon Collection - 3D Gallery</em></div>

	    <div class="row gallery animation-element slide-up" id="models">
		<?php
		$json_data = file_get_contents("models.json");
		$json_a = json_decode($json_data, true);
		//echo($json_data->models);
		$models = $json_a["models"];
		$first = true;
        foreach ($models as $model){
		    $animate = "scale";
		    if($first){ 
                $animate = "slide-up"; // first model slides instead of scales
                $first = false;
		    }
		    $link = "artifact.php?&id=". $model['id'];
		    $output = '';
		    $output .= '<div class="col-lg-4 col-md-6 col-sm-6 col-xsm-12 ' . $animate . ' gallery-div">';
		    $output .= '<div class="gallery-content-wrap">';
		    $output .= '<img src="../assets/images/grte/models/'. $model["image_name"] .  '" class="img-responsive">';
            $output .= '<div class="gallery-rollover">';
		    $output .= '<div class="table-display">';
		    $output .= '<div class="link-content">';
            
            $title = "";
            if(strlen($model["tribe"])>0){ // If tribe is not set do not add tribe and hyphen to title
                $title .= $model["tribe"] . " - ";
            }
            $title .= $model["object_type"];
		    $output .= '<h2><a href="'.$link.'">'. $title . '</a></h2>';
            $output .= '<p class="catalog">'. $model["id_title"] .'</p>';
		    $output .= '</div></div></div></div></div>';

		    print($output);
		}

		?>

	    </div>
	    <!-- /.row -->
	    <!-- 
	    <div class="row">
		<div class="col-lg-4 col-lg-offset-4">
		    <ul class="pagination">
			<li class="active"><a href="index.php">1</a></li>
			<li><a href="index_2.php">2</a></li>
		    </ul>
		</div>
	    </div> -->

	</div>
	<!-- /.marketing -->

	<?php include "includes/footer.php"; ?>
	<!-- Bootstrap core JavaScript
	     ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<?php include "includes/scripts.php"; ?>


	
    </body>

</html>
