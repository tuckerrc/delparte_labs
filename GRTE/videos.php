<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include "includes/head.php"; ?>
    </head>

    <body>

        <?php include "includes/nav.php"; ?>
        

	<div class="container">
            <!-- Three columns of text below the carousel -->
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h2> How to take photos for 3D models </h2>
                </div>
                <div class="col-md-6 col-md-offset-3 text-center">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe src="https://www.youtube.com/embed/w3QP0nS9_lQ" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h2> How to process photos for 3D models </h2>
                </div>
                <div class="col-md-6 col-md-offset-3 text-center">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe src="https://www.youtube.com/embed/30UGXrnEZ0o" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container -->


        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts.php"; ?>

    </body>

</html>
