<!DOCTYPE html>
<html lang="en">

    <head>
	<?php include "includes/head.php"; ?>
    </head>
    <!-- NAVBAR
	 ================================================== -->

    <body>
	<?php include "includes/nav.php"; ?>

	<!-- Marketing messaging and featurettes
	     ================================================== -->
	<!-- Wrap the rest of the page in another container to center all the content. -->

	<div class="container-fluid marketing">
	    <div class="container">
		<div class="row">
		    <div class="col-xs-8 col-xs-offset-2 text-center">
			<h3>3D Documentation and Visualization Techniques for Cultural Resources and Museum Collections</h3>
			<hr></hr>
		    </div>
		</div><!-- /.row -->

		<div class="row animation-element slide-right">
		    <div class="col-md-offset-1 col-sm-6 col-xs-12">
			<p>This collaboration between the National Park Service (NPS) museum and cultural resource staff, Native American Tribal Representatives, NPS museum professionals and conservators, and Idaho State University faculty and students offers innovative ways to adapt existing 3D technologies to preserve, document, visualize, and provide educational opportunities about cultural resources and museum collections in national parks. Our goal was to create a series of high­resolution 3D scans of museum objects from a wide range of Native American ethnographic objects in the David T. Vernon Collection from Grand Teton National Park, Wyoming.</p>
		    </div>
		    <div class="col-md-4 col-sm-6 col-sm-offset-0 col-xs-10 col-xs-offset-1 animation-element scale">
			<img src="../assets/images/sfm_screenshot.jpg" style="width: 100%;" />
		    </div>
		</div> <!-- /.row -->
		<div class="row">
		    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xsm-8 col-xsm-offset-2 text-center">
			<h3>The Team</h3>
			<hr></hr>
		    </div>
		</div>
		<div class="row">
		    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center animation-element slide-up">
			<div class="team-image  animation-element scale">
			    <img src="..\assets\images\Youngs.JPG" />
			</div>
			<br>
			<div class="team-info">
			    Project Director: Dr. Yolonda Youngs
			    <br>Project Co-PI
			</div>
		    </div>
		    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center animation-element slide-up">
			<div class="team-image animation-element scale">
			    <img src="..\assets\images\guilds.jpg" />
			</div>
			<br>
			<div class="team-info">
			    Project Co-Principle Investigator: Bridgette Guild
			    <br>Museum Curator/Tribal Liaison
			</div>
		    </div>
		    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center animation-element slide-up">
			<div class="team-image animation-element scale">
			    <img src="..\assets\images\delparte.jpg" />
			</div>
			<br>
			<div class="team-info">
			    Project Co-Investigator: Dr. Donna Delparte
			    <br>Idaho State University
			</div>
		    </div>
		</div>
		<div class="row">
		    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xsm-8 col-xsm-offset-2 text-center animation-element slide-up">
			<strong>Masters of Geographic Information Sciences Student Assistants: Tucker Chapman and Ashely Shields</strong>
			<hr></hr>
		    </div>
		</div><!-- /.row -->
		<div class="row">
		    <div class="col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2 col-xs-12 text-center animation-element slide-up">
			<div class="team-image animation-element scale">
			    <img src="..\assets\images\chapman.jpg" />
			</div>
			<br>
			<div class="team-info">
			    ISU Masters of Geographic Information Sciences Student Assistant:
			    <br>Tucker Chapman
			</div>
		    </div>
		    <div class="col-md-4 col-sm-4 col-xs-12 text-center animation-element slide-up">
			<div class="team-image animation-element scale">
			    <img src="..\assets\images\shields.jpg" />
			</div>
			<br>
			<div class="team-info">
			    ISU Masters of Geographic Information Sciences Student Assistant:
			    <br>Ashley Shields
			</div>
		    </div>
		</div>
	    </div> <!-- /.container -->
	</div>
	<!-- /.marketing -->

	<?php include "includes/footer.php"; ?>
	<!-- Bootstrap core JavaScript
	     ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<?php include "includes/scripts.php"; ?>
    </body>

</html>
