<!DOCTYPE html>
<html lang="en">

    <head>
	<?php include "includes/head.php"; ?>
    </head>
    <!-- NAVBAR
	 ================================================== -->

    <body>
	<?php include "includes/nav.php"; ?>

	<!-- Marketing messaging and featurettes
	     ================================================== -->
	<!-- Wrap the rest of the page in another container to center all the content. -->

	<div class="container-fluid marketing">

	    <?php
	    $json_data = file_get_contents("models.json");
	    $json_a = json_decode($json_data);
	    $models = $json_a->models;
	    //echo($json_data->models);

	    function filterModels($obj){
		return $obj->id == $_GET['id'];
	    }
	    $model_filtered = array_filter($models, "filterModels");
	    $model = array_values($model_filtered)[0];
	    
            $title = "";
            if(strlen($model->tribe)>0){ // If tribe is not set do not add tribe and hyphen to title
                $title .= $model->tribe . " - ";
            }
            $title .= $model->object_type;
	    ?>	    
            <!-- Three columns of text below the carousel -->
            <div class="container">
		<div class="row">
		    <div class="col-md-8">
			<div class="sketchfab-embed-wrapper">
			    <div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" width="640" hight="400" src="https://sketchfab.com/models/<?php echo $model->sketchfab_id; ?>/embed" frameborder="0" allowvr allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" onmousewheel=""></iframe></div>

			    <p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
				<a href="https://sketchfab.com/models/<?php echo $model->sketchfab_id; ?>?utm_medium=embed&utm_source=website&utm_campain=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;"><?php echo $title ?></a>
				by <a href="https://sketchfab.com/delpart?utm_medium=embed&utm_source=website&utm_campain=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Delparte Labs</a>
				on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campain=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
			    </p>
			</div>
		    </div>
		    <div class="col-md-4" >
			<?php if(strlen($model->tribe) > 0){ ?>
			    <h4>Tribe</h4>
			    <ul>
				<li><?php echo $model->tribe; ?></li>
			    </ul>
			<?php } ?>
			<?php if(strlen($model->object_type) > 0 ){ ?>
			<h4>Artifact Type</h4>
			<ul>
			    <li><?php echo $model->object_type; ?></li>
			</ul>
			<?php } ?>
			<?php if(strlen($model->id_title)>0){ ?>
			<h4>Catalog ID</h4>
			<ul>
			    <li><?php echo $model->id_title; ?></li>
			</ul>
			<?php } ?>
		    </div>
		    
		</div>

            </div><!-- /.container -->

	</div>
	<!-- /.marketing -->

	<?php include "includes/footer.php"; ?>
	<!-- Bootstrap core JavaScript
	     ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<?php include "includes/scripts.php"; ?>
    </body>

</html>
