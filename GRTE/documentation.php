<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include "includes/head.php"; ?>
    </head>

    <body>

        <?php include "includes/nav.php"; ?>
        

	<div class="container">
            <div class="row">
                <h1><strong>Instructions for acquiring and processing SFM Photos</strong></h1>
                <h2 id="getting-started">Getting Started</h2>
                <h3 id="camera-settings">Camera Settings</h3>
                <ul>
                    <li>Focal Length<ul>
                        <li>Use a focal length that you can feasibly manage to work with in your available space.</li>
                        <li>Nikon 18-55mm f/3.5-5.6G VR DX AF-S *roughly (18-24mm [f5-f8], 24-55mm [f8-f11]</li>
                        <li><a href="http://slrgear.com/reviews/zproducts/nikon18-55f35-56vr2/tloader.htm">Nikon 18-55mm f/3.5-5.6G VR II DX AF-S</a></li>
                    </ul>
                    </li>
                    <li>Aperture = 8-11<ul>
                        <li>Check the performance metric for the lens focal lengths to determine the optimal aperture size for the chosen focal length.</li>
                        <li>Best setting depends on lighting<ul>
                            <li>Aperture set to f/11: Room needs to be well lit. f/11 will achieve the greatest hyper focal distance to performance ratio on the Nikon 18-55 lens</li>
                            <li>Aperture set to f/8 will work if lighting is not ideal</li>
                        </ul>
                        </li>
                    </ul>
                    </li>
                    <li>ISO – adjust depending on lighting<ul>
                        <li>Good Light – Auto (Shouldn't matter in a well-lit room)</li>
                        <li>Low light – Lock at 100-200 (Lower is best)</li>
                    </ul>
                    </li>
                    <li>Metering: select 'Center Weighted' (see next page for directions)</li>
                    <li>Focus Mode: select 'AF-S' (Single-servo AF) (see next page for directions)</li>
                    <li>AF-Area Mode: select 'Single Point AF' (see next page for directions)</li>
                    <li>Exposure: select 'AEL' (autoexposure lock) (see next page for directions)<ul>
                        <li>Use autoexposure lock after first photo in series</li>
                    </ul>
                    </li>
                    <li>White Balance<ul>
                        <li>Each time settings or environment are changed<ul>
                            <li>photograph a sample of the object then click the AEL (autoexposure lock)</li>
                            <li>photograph a white balance card with the AEL activated</li>
                        </ul>
                        </li>
                    </ul>
                    </li>
                    <li>Photo Quantity<ul>
                        <li>Take many more photos than are necessary</li>
                        <li>Areas with lots of curvature need more photos<ul>
                            <li>many rows with close spacing sometimes</li>
                        </ul>
                        </li>
                    </ul>
                    </li>
                    <li>Verify Setting Functionality</li>
                </ul>
                <h3 id="photography-protocol">Photography Protocol</h3>
                <ul>
                    <li>Object
                        <ul>
                            <li>Fill camera field with object-only, as much as possible</li>
                            <li>Do not place full object in image frame</li>
                            <li>Object should be in focus</li>
                        </ul>
                    </li>
                </ul>
                
                <ul>
                    <li>Focus and Distance<ul>
                        <li>Don't get too close to object, allow it to take up about 2/3 of the frame. If the focus distance is to close, the depth of field will be very shallow and the entire object won't likely be in focus, the real trick is to get the object in focus and the background not in focus.</li>
                        <li>F8 is another good aperture setting that will make the background more blurry than at F11 if the object is in focus. Either way. Keep the aperture locked at whichever you decide to work with.</li>
                        <li>Generally, use the maximum focus (55mm) or minimum focus<ul>
                            <li>tape focus-ring, if necessary</li>
                            
                        </ul>
                        </li>
                    </ul></li>
                    <li>Maintain camera settings<ul>
                        <li>ISO, aperture, white balance, focus</li>


                    </ul></li>
                    <li>White balance reference: After settings are selected, take photo of white balance card</li>
                    <li>Take photos<ul>
                    <li>More photos than required is better than too few</li>
                    <li>7 sets around images (each with a particular viewing angle)<ul>
                        <li>See figures below for more details</li>
                    </ul>
                    </li>
                    <li>Additional rounds to document details<ul>
                        <li>Areas of interest, nooks, edges, seems, soles</li>
                        <li>Overlap of ~66%</li>
                    </ul>
                    </li>
                    <li>Ensure sufficient overlap at corners of subject</li>
                    <li>Add 10-15º of overlap at beginning/end of each round</li>
                    </ul></li>
                </ul>

                <h2 id="qaqc">QAQC</h2>
                <ul>
                    <li>Verify photo quality</li>
                    <li>Ensure entire object has been captured</li>
                    <li>Run alignment on low in Agisoft (workflow - align photos)<ul>
                        <li>Verify that there are no holes in object</li>
                    </ul>
                    </li>
                </ul>
                <h2 id="file-transfer">File Transfer</h2>
                <ul>
                    <li>Organize files following the template in the image below</li>
                    <li>Include companion Images<ul>
                        <li>Photo of object with its unique ID</li>
                        <li>'White Balance' image</li>
                    </ul>
                    </li>
                </ul>
                <h2 id="lightroom">Lightroom</h2>
                <ul>
                    <li>Import Photos</li>
                    <li>Select all to Change Settings (White balance = set to auto and Autotone = click button)</li>
                    <li>Export<ul>
                        <li>Click export -- Create 'Tiff' folder in appropriate file pathway</li>
                    </ul>
                    </li>
                </ul>
                <h2 id="agisoft-photoscan">Agisoft PhotoScan</h2>
                <ul>
                    <li>Save new document</li>
                    <li>Add photos (Workflow - Add Photos)</li>
                    <li>Assess Quality<ul>
                        <li>Set view to 'Details' -- Select all -- Right click any row --- Click "Estimate Image Quality"</li>
                    </ul>
                    </li>
                    <li>Detect Markers (Tools – Markers – Detect Markers)<ul>
                        <li>Settings = round 12 bit</li>
                        <li>Verify correctness – delete bad markers</li>
                    </ul>
                    </li>
                    <li>Align photos (Workflow – Align photos)<ul>
                        <li>For problem areas, select bad cameras and right click to "disable" them and align again</li>
                    </ul>
                    </li>
                    <li>Rotate model to appropriate XYZ axes (e.g. Footwear = toe down)</li>
                    <li>Build dense cloud (Workflow – build dense)</li>
                    <li>Build mesh (Workflow – build mesh)</li>
                    <li>Build texture (Workflow – build texture)</li>
                    <li>Build tiled model (Workflow – build tiled model)</li>
                </ul>
                <h3 id="products">Products</h3>
                <ul>
                    <li>Export as<ul>
                        <li>.obj</li>
                        <li>.PDF</li>
                    </ul>
                    </li>
                    <li>Upload<ul>
                        <li>Sketchfab</li>
                    </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /.container -->


        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts.php"; ?>

    </body>

</html>
