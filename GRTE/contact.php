<!DOCTYPE html>
<html lang="en">

    <head>
	<?php include "includes/head.php"; ?>
    </head>
    <!-- NAVBAR
	 ================================================== -->

    <body>
	<?php include "includes/nav.php"; ?>

	<!-- Marketing messaging and featurettes
	     ================================================== -->
	<!-- Wrap the rest of the page in another container to center all the content. -->

	<div class="container-fluid marketing">

            <!-- Three columns of text below the carousel -->
            <div class="container">
		<div class="row">
		    <h3>For more information please contact: </h3>
		    <hr>
		    <div class="col-xs-12">
			<strong>Dr. Yolonda Youngs</strong><br>
			<strong>Assistant Professor</strong><br>
			Department of Global Studies and Languages<br>
			Idaho State University<br>
			Pocatello, ID 83209, USA<br>
			<a href="mailto:younyolo@isu.edu">younyolo@isu.edu</a><br>
			<br>
			<a href="http://gisci.isu.edu/faculty/yolondaYoungs/">http://gisci.isu.edu/faculty/yolondaYoungs/</a><br>
			<a href="https://idahostate.academia.edu/YolondaYoungs">https://idahostate.academia.edu/YolondaYoungs</a>
		    </div>
		</div>
		<div class="row">
		    <h3>Presentations</h3>
		    <hr>
		    <div class="col-xs-12">
			<p><strong>Title: </strong><em>3D Documentation and Visualization Techniques for Cultural Resources and Museum Collections in Grand Teton National Park’s David T. Vernon Collection</em></p>
			<p><strong>Conference Title: </strong>13th Biennial Scientific Conference of the Greater Yellowstone Ecosystem</p>
			<p>Building on the Past, Leading into the Future: Sustaining the GYE in the Coming Century</p>
			<p>October 4-6, 2016</p>
			<a href="https://ww4.aievolution.com/ytc1601/index.cfm?do=cnt.page&pg=2008" target="_blank">Weblink</a>
		    </div>
		    <hr>
		</div>

            </div><!-- /.container -->

	</div>
	<!-- /.marketing -->

	<?php include "includes/footer.php"; ?>
	<!-- Bootstrap core JavaScript
	     ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<?php include "includes/scripts.php"; ?>
    </body>

</html>
