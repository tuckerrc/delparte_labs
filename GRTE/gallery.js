$( document ).ready(function() {
    $(".gallery").addClass("ready");
    $(".gallery-div").mouseover(function(){
      $(this).addClass("active");
    });
    $(".gallery-div").mouseout(function(){
      $(this).removeClass("active");
    });

});

// This script creates the nice animation for same page anchor links
// scroll handler
$(function() {
  var scrollToAnchor = function( id, event ) {
    // grab the element to scroll to based on the name
    var elem = $("a[name='"+ id +"']");
    // if that didn't work, look for an element with our ID
    if ( typeof( elem.offset() ) === "undefined" ) {
      elem = $("#"+id);
    }
    // if the destination element exists
    if ( typeof( elem.offset() ) !== "undefined" ) {
      // do the scroll
      console.log(elem);
      $('html, body').animate({
              scrollTop: elem.offset().top
      }, 1000 );
    } else {
      window.location = event.target.getAttribute("href");
      }
  };
    // bind to click event
    $("a").click(function( event ) {
      // only do this if it's an anchor link
      if ( $(this).attr("href").match("#") ) {
        // cancel default event propagation
        event.preventDefault();
        // scroll to the location
        var href = $(this).attr('href').split(/[#]+/).pop();
        scrollToAnchor( href, event );
      }
    });
  });
