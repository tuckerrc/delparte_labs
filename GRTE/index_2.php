<!DOCTYPE html>
<html lang="en">

<head>
<?php include "includes/head.php"; ?>
</head>
<!-- NAVBAR
================================================== -->

<body>
  <?php include "includes/nav.php"; ?>


    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img class="first-slide img-responsive" src="../assets/images/pouch_banner.jpg" alt="First slide">
                <div class="container">
                    <div class="carousel-caption">
                        <h3>Grand Teton National Park</h3>
                        <p>3D Models</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <img class="second-slide img-responsive" src="../assets/images/basket_banner.jpg" alt="Second slide">
                <div class="container">
                    <div class="carousel-caption">
                        <h3>Grand Teton National Park</h3>
                        <p>3D Models</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <img class="third-slide img-responsive" src="../assets/images/Grand_Teton_NP_VC.jpg" alt="Third slide">
                <div class="container">
                    <div class="carousel-caption">
                        <h3>Grand Teton National Park</h3>
                        <p>Visitor Center</p>
                        <p class="pull-right image-credit" ><a href="https://commons.wikimedia.org/wiki/File:Grand_Teton_NP_new_VC_GTNP1.jpg" target="_blank">Image: Wikimedia Commons</a></p>
                    </div>
                </div>
            </div>
            <!--<div class="item">
          <img class="third-slide" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
            </div>
          </div>
        </div>-->
        </div>
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- /.carousel -->


    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <a id="gallery"></a>

    <div class="container-fluid marketing">
        <!-- Three columns of text below the carousel -->

<div class="bar-header"><em>The Vernon Collection - 3D Gallery</em></div>

        <div class="row gallery animation-element slide-up">
          <!-- /.col-lg-4 -->
          <div id="first-gallery-div" class="col-lg-4 col-md-6 col-sm-6 col-xsm-12 slide-up gallery-div">
              <div class="gallery-content-wrap">
                  <img src="../assets/images/grte/pouch.jpg" class="img-responsive">
                  <div class="gallery-rollover">
                      <div class="table-display">
                          <div class="link-content">
                              <h2><a href="https://sketchfab.com/models/aa8375d5a63d4078907ef41bbde4ed9d" target="_blank">Sioux - Pouch</a></h2>
                              <p class="catalog">GRTE 5240</p>
                          </div>
                      </div>
                  </div>
              </div>

          </div>
          <!-- /.col-lg-4 -->
          <div class="col-lg-4 col-md-6 col-sm-6 col-xsm-12 scale gallery-div">
              <div class="gallery-content-wrap">
                  <img src="../assets/images/grte/5063.jpg" class="img-responsive">
                  <div class="gallery-rollover">
                      <div class="table-display">
                          <div class="link-content">
                              <h2><a href="https://sketchfab.com/models/0d6ccb13c846407ea3a506e0842903ee" target="_blank">Arapaho - Saddle Bag</a></h2>
                              <p class="catalog">GRTE 5063</p>
                          </div>
                      </div>
                  </div>
              </div>

          </div>
          <!-- /.col-lg-4 -->

          <div class="col-lg-4 col-md-6 col-sm-6 col-xsm-12 scale gallery-div">
              <div class="gallery-content-wrap">
                  <img src="../assets/images/grte/5110-right.jpg" class="img-responsive">
                  <div class="gallery-rollover">
                      <div class="table-display">
                          <div class="link-content">
                              <h2><a href="https://sketchfab.com/models/a69cab68f1634392938ea879f1da6f7a" target="_blank">Sioux	Moccasin</a></h2>
                            <p class="catalog">GRTE 5110 Right</p>
                          </div>
                      </div>
                  </div>
              </div>

          </div>
          <!-- /.col-lg-4 -->

          <div class="col-lg-4 col-md-6 col-sm-6 col-xsm-12 scale gallery-div">
              <div class="gallery-content-wrap">
                  <img src="../assets/images/grte/5110-left.jpg" class="img-responsive">
                  <div class="gallery-rollover">
                      <div class="table-display">
                          <div class="link-content">
                              <h2><a href="https://sketchfab.com/models/ad1990f0ed984cdcaa044ef5b84f1492" target="_blank">Sioux - Moccasin</a></h2>
                            <p class="catalog">GRTE 5110 Left</p>
                          </div>
                      </div>
                  </div>
              </div>

          </div>
          <!-- /.col-lg-4 -->

          <div class="col-lg-4 col-md-6 col-sm-6 col-xsm-12 scale gallery-div">
              <div class="gallery-content-wrap">
                  <img src="../assets/images/grte/6168_v2.jpg" class="img-responsive">
                  <div class="gallery-rollover">
                      <div class="table-display">
                          <div class="link-content">
                              <h2><a href="https://sketchfab.com/models/65970cac04ec43619f8317daeb809358" target="_blank">Cheyenne - Boy's Coat</a></h2>
                            <p class="catalog">GRTE 6168</p>
                            </div>
                      </div>
                  </div>
              </div>

          </div>
          <!-- /.col-lg-4 -->
          <div class="col-lg-4 col-md-6 col-sm-6 col-xsm-12 scale gallery-div">
              <div class="gallery-content-wrap">
                  <img src="../assets/images/grte/5104-left.jpg" class="img-responsive">
                  <div class="gallery-rollover">
                      <div class="table-display">
                          <div class="link-content">
                              <h2><a href="https://sketchfab.com/models/af362b8e34944719afadf753c00dfc7a" target="_blank">Arapaho -	Moccassin</a></h2>
                              <p class="catalog">GRTE 5104 Left</p>
                            </div>
                      </div>
                  </div>
              </div>

          </div>
          <!-- /.col-lg-4 -->
          <div class="col-lg-4 col-md-6 col-sm-6 col-xsm-12 scale gallery-div">
              <div class="gallery-content-wrap">
                  <img src="../assets/images/grte/5116-left.jpg" class="img-responsive">
                  <div class="gallery-rollover">
                      <div class="table-display">
                          <div class="link-content">
                              <h2><a href="https://sketchfab.com/models/bc3dbbcb8b6b4b7a9c33000192616308" target="_blank">Left Moccasin</a></h2>
                              <p class="catalog">GRTE 5116 Left</p>
                            </div>
                      </div>
                  </div>
              </div>

          </div>
          <!-- /.col-lg-4 -->

                    <div class="col-lg-4 col-md-6 col-sm-6 col-xsm-12 scale gallery-div">
              <div class="gallery-content-wrap">
                  <img src="../assets/images/grte/5055.jpg" class="img-responsive">
                  <div class="gallery-rollover">
                      <div class="table-display">
                          <div class="link-content">
                              <h2><a href="https://sketchfab.com/models/bc3dbbcb8b6b4b7a9c33000192616308" target="_blank">Sioux - Parfleche</a></h2>
                              <p class="catalog">GRTE 5055</p>
                            </div>
                      </div>
                  </div>
              </div>

          </div>
          <!-- /.col-lg-4 -->
          
        <div class="col-lg-4 col-md-6 col-sm-6 col-xsm-12 scale gallery-div">
              <div class="gallery-content-wrap">
                  <img src="../assets/images/grte/6170.jpg" class="img-responsive">
                  <div class="gallery-rollover">
                      <div class="table-display">
                          <div class="link-content">
                              <h2><a href="https://sketchfab.com/models/aa8bde0b1d7246e18230d40616e32010" target="_blank">Sioux - Jacket</a></h2>
                              <p class="catalog">GRTE 6170</p>
                            </div>
                      </div>
                  </div>
              </div>

        </div>
          <!-- /.col-lg-4 -->
          
        <div class="col-lg-4 col-md-6 col-sm-6 col-xsm-12 scale gallery-div">
              <div class="gallery-content-wrap">
                  <img src="../assets/images/grte/6165.jpg" class="img-responsive">
                  <div class="gallery-rollover">
                      <div class="table-display">
                          <div class="link-content">
                              <h2><a href="https://sketchfab.com/models/2ec7b524b52c42bea25f157cf105e0c4" target="_blank">Jacket</a></h2>
                              <p class="catalog">GRTE 6165</p>
                            </div>
                      </div>
                  </div>
              </div>

        </div>
          <!-- /.col-lg-4 -->
          
        <div class="col-lg-4 col-md-6 col-sm-6 col-xsm-12 scale gallery-div">
              <div class="gallery-content-wrap">
                  <img src="../assets/images/grte/5144.jpg" class="img-responsive">
                  <div class="gallery-rollover">
                      <div class="table-display">
                          <div class="link-content">
                              <h2><a href="https://sketchfab.com/models/86c52755ecb94c0b9c5fc627a788f504" target="_blank">Beadwork</a></h2>
                              <p class="catalog">GRTE 5144</p>
                            </div>
                      </div>
                  </div>
              </div>

        </div>
          <!-- /.col-lg-4 -->          

        <div class="col-lg-4 col-md-6 col-sm-6 col-xsm-12 scale gallery-div">
              <div class="gallery-content-wrap">
                  <img src="../assets/images/grte/5268.jpg" class="img-responsive">
                  <div class="gallery-rollover">
                      <div class="table-display">
                          <div class="link-content">
                              <h2><a href="https://sketchfab.com/models/afedf353eff24b0a841e8d17bc1bad2c" target="_blank">Beadwork Pouch</a></h2>
                              <p class="catalog">GRTE 5268</p>
                            </div>
                      </div>
                  </div>
              </div>

        </div>
          <!-- /.col-lg-4 -->

        <div class="col-lg-4 col-md-6 col-sm-6 col-xsm-12 scale gallery-div">
              <div class="gallery-content-wrap">
                  <img src="../assets/images/grte/5131-left.jpg" class="img-responsive">
                  <div class="gallery-rollover">
                      <div class="table-display">
                          <div class="link-content">
                              <h2><a href="https://sketchfab.com/models/7a9ecedb1fe945668c5ae6bc9ff29615" target="_blank">Moccasin</a></h2>
                              <p class="catalog">GRTE 5131</p>
                            </div>
                      </div>
                  </div>
              </div>

        </div>
          <!-- /.col-lg-4 -->

          <div class="col-lg-4 col-md-6 col-sm-6 col-xsm-12 scale gallery-div">
              <div class="gallery-content-wrap">
                  <img src="../assets/images/grte/5118.jpg" class="img-responsive">
                  <div class="gallery-rollover">
                      <div class="table-display">
                          <div class="link-content">
                              <h2><a href=https://sketchfab.com/models/46b51c42efe3427082ba39496820ae2d" target="_blank">Moccasin</a></h2>
                              <p class="catalog">GRTE 5118</p>
                            </div>
                      </div>
                  </div>
              </div>

        </div>
          <!-- /.col-lg-4 -->
          
                  <div class="col-lg-4 col-md-6 col-sm-6 col-xsm-12 scale gallery-div">
              <div class="gallery-content-wrap">
                  <img src="../assets/images/grte/5131-right.jpg" class="img-responsive">
                  <div class="gallery-rollover">
                      <div class="table-display">
                          <div class="link-content">
                              <h2><a href=https://sketchfab.com/models/b644948f395342a3967739a9df8f1dee" target="_blank">Moccasin</a></h2>
                              <p class="catalog">GRTE 5131</p>
                            </div>
                      </div>
                  </div>
              </div>

        </div>
          <!-- /.col-lg-4 -->

        </div>
        <!-- /.row -->
        <div class="row">
          <div class="col-lg-4 col-lg-offset-4">
            <ul class="pagination">
               <li><a href="index.php">1</a></li>
               <li class="active"><a href="index_2.php">2</a></li>
            </ul>
          </div>
        </div>
    </div>
    <!-- /.marketing -->


    <?php include "includes/footer.php"; ?>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <?php include "includes/scripts.php"; ?>

</body>

</html>
