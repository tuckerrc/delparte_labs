<!-- FOOTER -->
<!-- FOOTER -->
<div class="container-fullwidth footer">
  <div class="container">
    <footer>
      <p><em>Project funding provided by a U.S. DOI grant through the NPS's National Center for Preservation Technology and Training (NCPTT) program.<br>
      Award  #P15A00095</em></p>
    </footer>
  </div>
</div>

<!-- 'Back to top' arrow that shows in the bottom right corner -->
<a href="#top" class="back-to-top">
 <span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
</a>
