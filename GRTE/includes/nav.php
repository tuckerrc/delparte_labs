<a id="top"></a>
<div class="container-fullwidth header-nav">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-8 col-xs-6">
        <div class="header-logo-div">
          <a href="http://isu.edu"><img src="../assets/images/isu_logo_white.png" /></a>
        </div>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 pull-right">
        <div class="isu-div">
          <a href="http://geoviz.geology.isu.edu/delparte_labs/"><em>Delparte Labs</em></a><br>
          <img src="../assets/images/discover-foot.png" />
        </div>
      </div>
      <div class="col-md-8 col-xs-12">
        <div class="row">
          <div class="col-xs-10">
            <div class="project-title">
              <p>Grand Teton National Park</p>
              <p><em>&nbsp;The David T. Vernon Collection - 3D Documentation Project</em></p>
            </div>
          </div>
          <div class="col-xs-2">
            <img src="../assets/images/nps_logo_sm.png" class="pull-right">
          </div>
        </div>
        <hr></hr>
        <nav class="navbar navbar-inverse navbar-static-top">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                  <li id="index"><a href="index.php">Home</a></li>
                  <li><a href="index.php#gallery">Gallery</a></li>
                  <li id="dropdown">
                      <a href="#" class="dropdown-toggle"  data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Instructions <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                          <li id="videos"><a href="videos.php">Videos</a></li>
                          <li id="documentation"><a href="documentation.php">Documentation</a></li>
                      </ul>
                  </li>

                  <li id="team" ><a href="team.php">Team</a></li>
                  <li id="contact" ><a href="contact.php">Contact</a></li>
              </ul>
          </div>
        </nav>
      </div>

    </div>

  </div>
</div>
