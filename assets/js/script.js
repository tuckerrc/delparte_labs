$(document).ready(function() {
    var path = window.location.pathname;
    try {
        var filename = path.match(/.*\/([^/]+)\.([^?]+)/i)[1];
    } catch (e) {
        if (e instanceof TypeError) {
            var filename = "index";
        }
    }
    console.log(filename)
    setTimeout(function() {
      if (filename === "index_2"){
        filename = "index";
      }
        var activePage = document.getElementById(filename)

        console.log(activePage);
        activePage.className = 'active';
    }, (250));
});

var $animation_elements = $('.animation-element');
var $window = $(window);

function check_if_in_view() {
    var window_height = $window.height();
    var window_top_position = $window.scrollTop();
    var window_bottom_position = (window_top_position + window_height);

    $.each($animation_elements, function() {
        var $element = $(this);
        var element_height = $element.outerHeight();
        var element_top_position = $element.offset().top;
        var element_bottom_position = (element_top_position + element_height);

        //check to see if this current container is within viewport
        if ((element_bottom_position >= window_top_position) &&
            (element_top_position <= window_bottom_position)) {
            $element.addClass('in-view');
        } else {
            //$element.removeClass('in-view');
        }
    });
}

$window.on('scroll resize', check_if_in_view);
$window.trigger('scroll');


$( document ).ready(function() {
    $(".gallery").addClass("ready");
    $(".gallery-div").mouseover(function(){
      $(this).addClass("active");
    });
    $(".gallery-div").mouseout(function(){
      $(this).removeClass("active");
    });

});

// This script creates the nice animation for same page anchor links
// scroll handler
$(function() {
  var scrollToAnchor = function( id, event ) {
    // grab the element to scroll to based on the name
    var elem = $("a[name='"+ id +"']");
    // if that didn't work, look for an element with our ID
    if ( typeof( elem.offset() ) === "undefined" ) {
      elem = $("#"+id);
    }
    // if the destination element exists
    if ( typeof( elem.offset() ) !== "undefined" ) {
      // do the scroll
      console.log(elem);
      $('html, body').animate({
              scrollTop: elem.offset().top
      }, 1000 );
    } else {
      window.location = event.target.getAttribute("href");
      }
  };
    // bind to click event
    $("a").click(function( event ) {
      // only do this if it's an anchor link
      if ( $(this).attr("href").match("#") ) {
        // cancel default event propagation
        event.preventDefault();
        // scroll to the location
        var href = $(this).attr('href').split(/[#]+/).pop();
        scrollToAnchor( href, event );
      }
    });
  });


  jQuery(document).ready(function() {
      var offset = 250;
      var duration = 300;
      jQuery(window).scroll(function() {
          if (jQuery(this).scrollTop() > offset) {
              jQuery('.back-to-top').fadeIn(duration);
          } else {
              jQuery('.back-to-top').fadeOut(duration);
          }
      });

      jQuery('.back-to-top').click(function(event) {
          event.preventDefault();
          jQuery('html, body').animate({
              scrollTop: 0
          }, duration);
          return false;
      })
  });
