<!-- FOOTER -->
<!-- FOOTER -->
<div class="container-fullwidth footer">
  <div class="container">
    <footer>
      <p><em>Project funding acknowledgement<br>
      Award  #XXXXXXXXX</em></p>
    </footer>
  </div>
</div>

<!-- 'Back to top' arrow that shows in the bottom right corner -->
<a href="#top" class="back-to-top">
 <span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
</a>
