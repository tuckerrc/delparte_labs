<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "includes/head.php"; ?>
</head>

<body>

  <?php include "includes/nav.php"; ?>
  

    <div class="container content">

        <div class="starter-template">
            <h1>Bootstrap starter template</h1>
            <p class="lead">Use this document as a way to quickly start any new project.
                <br> All you get is this text and a mostly barebones HTML document.</p>
        </div>

    </div>
    <!-- /.container -->


            <?php include "includes/footer.php"; ?>
            <?php include "includes/scripts.php"; ?>

</body>

</html>
