<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "includes/head.php"; ?>
</head>

<body>

  <?php include "includes/nav.php"; ?>


<div class="jumbotron">
  <div class="container">
    <h1>Graduates</h1>
  </div>
</div>
    <div class="container content students">

      <div class="row animation-element slide-right">
        <div class="col-lg-6 col-md-6">
          <h2>Brock Lipple</h2>
          <h3>M.S. in Geographic Information Science</h3>
          <p>Using his classroom and research experience in the M.S. in Geographic Information Science program at ISU, Brock has gone on to do some exciting work. He is now the GIS Director of Empire Unmanned that is one of the leading providers of UAV data products in Idaho, servicing clients in agriculture, mining, and research.</p>
        </div>
        <div class="col-lg-5 col-md-5 col-md-offset-1 col-lg-offset-1">
          <img src="assets/images/brock_sm.jpg" class="center-block img-responsive">
        </div>
      </div>


      <div class="row animation-element slide-left">
        <div class="col-lg-6 col-md-6">
          <h2>Joel Johansen</h2>
          <h3>M.S. in Geographic Information Science</h3>
          <p>Joel Johansen completed his Masters in GIS in 2015 and is currently working as a Precision Agriculture Specialist for the J. R. Simplot company. His job includes database management, data processing, software development, and analyzing satellite imagery to monitor crop health.</p>
        </div>
        <div class="col-lg-5 col-md-5 col-md-offset-1 col-lg-offset-1">
          <img src="assets/images/joel_sm.jpg" class="center-block img-responsive">
        </div>
      </div>

      <div class="row animation-element slide-right">
        <div class="col-lg-6 col-md-6">
          <h2>Mike Griffel</h2>
          <h3>M.S. in Geographic Information Science</h3>
          <p>Mike Griffel worked for several years in the local agricultural sector before returning to graduate school in the MSGIS program. For his thesis he studied field variability and the use of hyperspectral data for crop threat detection. Upon graduation he was hired by Idaho National Labs to work in their Biofuels and Renewable Energy Technology Department as an Agricultural Research Analyst applying his expertise in satellite/UAS data processing.</p>
        </div>
        <div class="col-lg-5 col-md-5 col-md-offset-1 col-lg-offset-1">
          <img src="assets/images/mike_griffel_sm.jpg" class="center-block img-responsive">
        </div>
      </div>

      <div class="row animation-element slide-left">
        <div class="col-lg-6 col-md-6">
          <h2>Matthew Belt</h2>
          <h3>M.S. in Geographic Information Science</h3>
          <p>Matt Belt started up his own consulting service as a geospatial engineer based in beautiful Whitefish, MT. He continues to participate on many projects for Delparte Labs, including, a project to use UAS to determine prairie dog colony size and density in north-central Montana. (Photo – first from Right)</p>
        </div>
        <div class="col-lg-5 col-md-5 col-md-offset-1 col-lg-offset-1">
          <img src="assets/images/ISU_team_lookout.jpg" class="center-block img-responsive">
        </div>
      </div>


    </div><!-- /.container -->

            <?php include "includes/footer.php"; ?>
            <?php include "includes/scripts.php"; ?>
  </body>
</html>
