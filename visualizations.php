<!DOCTYPE html>
<html lang="en">
    <head>
	<?php include "includes/head.php"; ?>
    </head>

    <body>

	<?php include "includes/nav.php"; ?>


	<div class="jumbotron">
	    <div class="container">
		<h1>Visualizations</h1>
	    </div>
	</div>

	<div class="container content">
	    <h3 class="animation-element fade-in">Virtual Tours</h3>
            <div class="row">
		<div class="col-sm-3 col-xs-5 animation-element slide-up">
		    <?php $link = 'http://miles.isu.edu/Greenway/Greenway.html'; ?>
		    <a href="<?php echo $link; ?>" target="_blank"><img src="assets/images/portneuf_greenway.jpg" class="img-responsive"></a>
		    <div class="caption"><a href="<?php echo $link; ?>" target="_blank">Portneuf Greenway</a></div>
		    </a>
		</div>
		<div class="col-sm-3 col-xs-5 col-xs-offset-1 animation-element slide-up timed-1">
		    <?php $link = 'http://geoviz.geology.isu.edu/VirtualTours/RISE/isuRISE.html'; ?>
		    <a href="<?php echo $link; ?>" target="_blank"><img src="assets/images/rise_building.jpg" class="img-responsive"></a>
		    <div class="caption"><a href="<?php echo $link; ?>" target="_blank">RISE Building</a></div>
		</div>
		<div class="col-sm-3 col-xs-5 col-sm-offset-1 col-xs-offset-0 animation-element slide-up timed-2">
		    <?php $link = 'http://www2.cose.isu.edu/~delparte/gigapans/glacier/gnp.html'; ?>
		    <a href="<?php echo $link; ?>" target="_blank"><img src="assets/images/gigapans.jpg" class="img-responsive"></a>
		    <div class="caption"><a href="<?php echo $link; ?>" target="_blank">GeoViz Group Gigapans</a></div>
		</div>
            </div>

	    <h3 class="animation-element fade-in">City Engine Models</h3>
	    <div class="row">
		<div class="col-sm-3 col-xs-5 animation-element slide-up">
		    <?php $link = 'http://miles.giscenter.isu.edu/swipe/index.html'; ?>
		    <a href="<?php echo $link; ?>" target="_blank"><img src="assets/images/portneuf_river_1959.jpg" class="img-responsive"></a>
		    <div class="caption"><a href="<?php echo $link; ?>" target="_blank">Portneuf River 1959</a></div>
		    </a>
		</div>
		<div class="col-sm-3 col-xs-5 col-xs-offset-1 animation-element slide-up timed-1">
		    <?php $link = 'http://bit.ly/1LdZoNT'; ?>
		    <a href="<?php echo $link; ?>" target="_blank"><img src="assets/images/portneuf_river_2035.jpg" class="img-responsive"></a>
		    <div class="caption"><a href="<?php echo $link; ?>" target="_blank">Pocatello Future Downtown</a></div>
		</div>
		<div class="col-sm-3 col-xs-5 col-sm-offset-1 col-xs-offset-0 animation-element slide-up timed-2">
		    <?php $link = 'http://bit.ly/1sYTona'; ?>
		    <a href="<?php echo $link; ?>" target="_blank"><img src="assets/images/pocatello_3d.jpg" class="img-responsive"></a>
		    <div class="caption"><a href="<?php echo $link; ?>" target="_blank">Pocatello 3D</a></div>
		</div>
	    </div>

	    <h3 class="animation-element fade-in">Historic Orthoimagery</h3>
            <div class="row">
		<div class="col-sm-3 col-xs-5 animation-element slide-up">
		    <?php $link = 'http://miles.isu.edu/Basemap/HistoricPocatello.html'; ?>
		    <a href="<?php echo $link; ?>" target="_blank"><img src="assets/images/historical_imagery.jpg" class="img-responsive"></a>
		    <div class="caption"><a href="<?php echo $link; ?>" target="_blank">Pocatello Historic Imagery</a></div>
		</div>
		<div class="col-sm-3 col-xs-5 col-xs-offset-1 animation-element slide-up timed-1">
		    <?php $link = 'http://miles.giscenter.isu.edu/swipe/index.html'; ?>
		    <a href="<?php echo $link; ?>" target="_blank"><img src="assets/images/compare_pocatello_history.jpg" class="img-responsive"></a>
		    <div class="caption"><a href="<?php echo $link; ?>" target="_blank">Compare Pocatello 1959 and 2013</a></div>
		    </a>
		</div>

		<div class="col-sm-3 col-xs-5 col-sm-offset-1 col-xs-offset-0 animation-element slide-up timed-2">
		    <?php $link = 'http://miles.giscenter.isu.edu/kalaupapa/overview.html'; ?>
		    <a href="<?php echo $link; ?>" target="_blank"><img src="assets/images/kalaupapa_overview.jpg" class="img-responsive"></a>
		    <div class="caption"><a href="<?php echo $link; ?>" target="_blank">Kalaupapa National Historical Park</a>
			<!--<a href="http://miles.giscenter.isu.edu/kalaupapa/overview_culture.html" target="_blank">Cultural</a>--></div>
		</div>
            </div>
	</div>
	<!-- /.container -->


        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts.php"; ?>

</body>

</html>
