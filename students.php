<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "includes/head.php"; ?>
</head>

<body>

  <?php include "includes/nav.php"; ?>


    <div class="jumbotron">
      <div class="container">
        <h1>Current Students</h1>
      </div>
    </div>
    <div class="container students">


      <div class="row animation-element slide-right">
        <div class="col-lg-6 col-md-6">
          <h2>Ashley Shields</h2>
          <h3>M.S. in Geographic Information Science</h3>
          <p>Ashley Shields utilizes GIS, particularly UAS (unmanned areal systems) and satellite image processing techniques, as well as LiDAR data analysis, to remotely detect geologic structures in eastern Idaho. She is originally from northern California, where she received her bachelor’s degree in Geology before moving to Pocatello to pursue a master’s degree in Geographic Information Science.</p>
        </div>
        <div class="col-lg-5 col-md-5 col-md-offset-1 col-lg-offset-1">
          <img src="assets/images/ashley_shields.jpg" class="center-block img-responsive">
        </div>
      </div>

      <div class="row animation-element slide-left">
        <div class="col-lg-6 col-md-6">
          <h2>Jared Ogle</h2>
          <h3>M.S. in Geographic Information Science</h3>
          <p>Jared Ogle originally comes from Georgia, where he received his undergraduate degree and a Master’s in Urban and Regional Planning. He moved to Pocatello to pursue further studies in Geographic Information Science for the MILES (Managing Idaho’s Landscapes and Ecosystem Services) project at ISU and has been using his planning background and geospatial knowledge to quantify the sustainability in growth for Pocatello.</p>
        </div>
        <div class="col-lg-5 col-md-5 col-md-offset-1 col-lg-offset-1">
          <img src="assets/images/jared.jpg" class="center-block img-responsive">
        </div>
      </div>

      <div class="row animation-element slide-right">
        <div class="col-lg-6 col-md-6">
          <h2>Tucker Chapman</h2>
          <h3>M.S. in Geographic Information Science</h3>
          <p>Tucker Chapman is developing a Climate Station Interpolation Toolkit for the watershed research and analysis.  He is using GIS to automate the interpolation and cross validation process while providing a simple user interface for researchers to use.  His work is part of the WC-WAVE project studying mountain watersheds in Idaho, Nevada, and New Mexico.</p>
        </div>
        <div class="col-lg-5 col-md-5 col-md-offset-1 col-lg-offset-1">
          <img src="assets/images/tucker_sm.jpg" class="center-block img-responsive">
        </div>
      </div>

      <div class="row animation-element slide-left">
        <div class="col-lg-6 col-md-6">
          <h2>Hanan Abou Ali </h2>
          <h3>M.S. in Geographic Information Science</h3>
          <p>Hanan Abou Ali is a Fullbright Scholar researching precision agriculture techniques studying potato crops for threat detection and irrigation management. She is using unmanned aircraft systems data and sentinel satellite imagery to explore efficient methods between Idaho and Lebanon. </p>
        </div>
        <div class="col-lg-5 col-md-5 col-md-offset-1 col-lg-offset-1">
          <img src="assets/images/hanan_crop.jpg" class="center-block img-responsive">
        </div>
      </div>


    </div><!-- /.container -->

    <?php include "includes/footer.php"; ?>
    <?php include "includes/scripts.php"; ?>

  </body>
</html>
