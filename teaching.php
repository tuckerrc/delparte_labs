<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "includes/head.php"; ?>
</head>

<body>

  <?php include "includes/nav.php"; ?>


<div class="jumbotron">
    <div class="container">
        <h1>Teaching</h1>
    </div>
</div>

    <div class="container content">
      <div class="row">
        <div class="col-md-7">
          <h2>Principles of Geographical Information System: GEOL 4403/5503 – 3 semester hours.</h2>
          <p>Study of GIS fundamentals, coordinate systems, vector and raster data analysis, introduction to GPS, geodatabases, and metadata. Practical application of Esri GIS software. Build, edit, and query a GIS; basic spatial analysis. Requires competence in computer operating systems. COREQ: GEOL 5503L</p>

          <h2>Principles of GIS Laboratory: GEOL 4403/5503L – 0 semester hours.</h2>
          <p>Computer lab assignments to apply principles from GEOL 4403/5503. COREQ: GEOL 4403/5503</p>

<!--          <h2>Geovisualization (GEOL 5599)</h2>
          <p>Geographic Visualization facilitates the interactive real-time display of geospatial data both spatially and temporally. This data can be the result of geodata analysis or data exploration of existing datasets. The power of geovisualization is the ability to allow users to visualize data in new ways that allows for scientific discovery and improved decision-making. This course explores various techniques, tools and the theory behind geovisualization. Students will have the opportunity to review relevant literature and experiment with new hardware and software visualization tools. <br><strong>Level</strong>: Graduate</p>
-->
          <h2>Geotechnology Seminar: GEOL 4408/5508 – 2 semester hours</h2>
          <p>GIS applications in natural and social sciences, ethical and legal issues, current status and recent advances in GeoTechnology. Prepare abstracts, thesis prospecti, literature reviews, and presentations and learn to manage references.</p>

          <h2>Advanced Geographic Information Systems: GEOL 5504 – 3 semester hours.</h2>
          <p>Study of relational geodatabases, including spatial analysis, modeling, and remote sensing. Practical application of GIS software as well as open source GIS. Exercises include digitizing, querying, digital terrain modeling, and image processing. </p>

          <h2>Web Mapping with JavaScript: GEOL 5580 – 3 semester hours</h2>
          <p>Explore web mapping using Esri’s JavaScript API, ArcGIS Online, Google API and other open source tools. Create visualizations for geo applications in the form of Augmented Reality and other 3D visualizations. Build skills and knowledge to create customized web mapping applications, mobile based apps, and advanced visualizations.</p>

          <h2>Geostatistics – Spatial Data Analysis and Modeling: GEOL 6608 – 3 semester hours</h2>
          <p>Description, analysis and modeling of spatial data, emphasizing hands-on application of geostatistical software tools for spatial analysis and probabilistic modeling in the geosciences.</p>

          <h2>Unmanned Aircraft Systems (UAS) Applications for the Geosciences: GEOL 6611 – 3 semester hours</h2>
          <p>Unmanned Aircraft Systems (UAS) are on the rise as a tool for geoscientists to collect remotely sensed data. Topics will cover the varied applications and regulations of UAS. Learn the workflow of UAS data collection, processing and analysis approaches. Determine which UAS platform and sensor is appropriate to geoscience research questions. Includes field demos and hands-on learning.</p>


          <h2>Esri's Training Resources</h2>
          <p><a href="http://training.esri.com/gateway/index.cfm" target="_blank">Web Based Training</a></p>
        </div>
        <div class="col-md-4 col-md-offset-1">
          <img src="assets/images/teaching_pic.jpg" class="img-responsive center-block">
          <h3>Geotechnology Program</h3>
          <p><a href="http://isu.edu/gisci/programs/" target="_blank">Degrees Offered in GIS</a></p>
          <h3>Dept. of Geosciences</h3>
          <p><a href="http://geology.isu.edu/"  target="_blank">Explore course offerings in the geosciences </a></p>
          <h3>GIS TReC</h3>
          <p><a href="http://giscenter.isu.edu/index.htm" target="_blank">GIS Training and Resources</a></p>
          <h3><a href="http://isu.edu/apply/" target="_blank">Apply Now</h3>
        </div>
      </div>

    </div><!-- /.container -->
    <?php include "includes/footer.php"; ?>
    <?php include "includes/scripts.php"; ?>
  </body>
</html>
