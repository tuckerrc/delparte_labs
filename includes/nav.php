<a id="top"></a>
<div class="container-fullwidth header-nav">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-8 col-xs-6">
        <div class="header-logo-div">
          <a href="http://isu.edu"><img src="assets/images/isu_logo_white.png" /></a>
        </div>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 pull-right">
        <div class="isu-div">
          <a href="http://geoviz.geology.isu.edu/delparte_labs/"><em>Delparte Labs</em></a><br>
          <img src="assets/images/discover-foot.png" />
        </div>
      </div>
      <div class="col-md-8 col-xs-12">
        <div class="row">
          <div class="col-xs-12">
            <div class="project-title">
              <p>Donna Delparte</p>
              <p><em>&nbsp;Assistant Professor, Department of Geosciences</em></p>
            </div>
          </div>
        </div>
        <hr></hr>
        <nav class="navbar navbar-inverse navbar-static-top">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li id="index"><a href="index.php">Home</a></li>
              <li id="research"><a href="research.php">Research</a></li>
              <li id="teaching"><a href="teaching.php">Teaching</a></li>
              <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Projects <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                      <li id="climate-interpolation"><a href="/delparte_labs/VWCSIT/">WCWAVE Climate Interpolation</a></li>
                      <li id="grand-tetons-np"><a href="/delparte_labs/GRTE/">Grand Tetons 3D</a></li>
                      <li id="kalaupapa-nat-his-park"><a href="http://miles.giscenter.isu.edu/kalaupapa/overview_culture2.html">Kalaupapa Nat. His. Park</a></li>
                      <li id="avalanche"><a href="/delparte_labs/avalanche.php">Avalanche Simulation</a></li>
                  </ul>
              </li>
              <!--<li id="outreach"><a href="outreach.php">Outreach</a></li>-->
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Students <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li id="students"><a href="students.php">Current</a></li>
                  <li id="graduates"><a href="graduates.php">Graduates</a></li>

                </ul>
              </li>
              <!--<li id="demos"><a href="demos.php">Demos</a></li>-->
              <!--<li id="data"><a href="data.php">Data</a></li>-->
              <li id="visualizations"><a href="visualizations.php">Visualizations</a></li>

              <li id="about"><a href="about.php">About</a></li>
            </ul>
          </div>
        </nav>
      </div>

    </div>

  </div>
</div>
