<footer class="footer">
  <div class="container">
    <p class="text-muted"><span class="glyphicon glyphicon-copyright-mark" aria-hidden="true"></span> 2016 Donna Delparte ~ All rights reserved.</p>
  </div>
</footer>

<!-- 'Back to top' arrow that shows in the bottom right corner -->
<a href="#top" class="back-to-top">
 <span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
</a>
