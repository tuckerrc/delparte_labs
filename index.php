<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "includes/head.php"; ?>
</head>

<body>

  <?php include "includes/nav.php"; ?>

    <div class="jumbotron front_page">
      <div class="container">
        <h1>Donna Delparte</h1>
        <p>Assistant Professor, Department of Geosciences</p>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-lg-8 col-md-8 col-md-offset-2">
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
              <li data-target="#myCarousel" data-slide-to="3"></li>
              <li data-target="#myCarousel" data-slide-to="4"></li>
              <li data-target="#myCarousel" data-slide-to="5"></li>
              <li data-target="#myCarousel" data-slide-to="6"></li>
              <li data-target="#myCarousel" data-slide-to="7"></li>

            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
              <div class="item active">
                <img src="assets/images/drones.jpg" alt="Donna Delparte, PhD" class="img-responsive center-block">
                <div class="carousel-caption">
                  <a href="#"><h3>Donna Delparte, PhD</h3></a>
                </div>
              </div>

              <div class="item">
                <img src="assets/images/ISU_team_lookout.jpg" alt="Reseach by Donna Delparte, PhD" class="img-responsive center-block">
                <div class="carousel-caption">
                  <a href="research.html"><h3>Research</h3></a>
                </div>
              </div>

              <div class="item">
                <img src="assets/images/teaching_pic.jpg" alt="Teaching with Donna Delparte, PhD" class="img-responsive center-block">
                <div class="carousel-caption">
                  <a href="teaching.html"><h3>Teaching</h3></a>
                </div>
              </div>

              <div class="item">
                <img src="assets/images/PrecisionHawk.jpg" alt="Students of Donna Delparte, PhD" class="img-responsive center-block">
                <div class="carousel-caption">
                  <a href="students.html"><h3>Students</h3></a>
                </div>
              </div>

              <div class="item">
                <img src="assets/images/outreachpic.jpg" alt="Outreach" class="img-responsive center-block">
                <div class="carousel-caption">
                  <a href="outreach.html"><h3>Outreach</h3></a>
                  <p>Virtual field trips and more...</p>
                </div>
              </div>

              <div class="item">
                <img src="assets/images/demopic.jpg" alt="Demo" class="img-responsive center-block">
                <div class="carousel-caption">
                  <a href="demo.html"><h3>Demo</h3></a>
                </div>
              </div>

              <div class="item">
                <img src="assets/images/datapic.jpg" alt="Data" class="img-responsive center-block">
                <div class="carousel-caption">
                  <a href="data.html"><h3>Data</h3></a>
                  <p>Data links and resources...</p>
                </div>
              </div>

              <div class="item">
                <img src="assets/images/aboutpic.jpg" alt="About" class="img-responsive center-block">
                <div class="carousel-caption">
                  <a href="outreach.html"><h3>About</h3></a>
                  <p>Contact Donna Delparte...</p>
                </div>
              </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
        <!--<div class="col-lg-4 col-md-4">
          <ul class="front-list">
            <li><h4><a href="research.html">Research</a></h4></li>
              <ul>
                <li>Avalanches &amp; Mass Movements</li>
                <li>LiDAR, SfM, &amp; Photogrammetry</li>
                <li>Landscape Change</li>
              </ul>
            <li><h4><a href="teaching.html">Teaching</a></h4></li>
              <ul>
                <li>Spring Courses</li>
                <li>Fall Courses</li>
                <li>Seminars</li>
              </ul>
            <li><h4><a href="outreach.html">Outreach</a></h4></li>
              <ul>
                <li>Virtual Field Trips</li>
                <li>Videos</li>
              </ul>
            <li><h4><a href="students.html">Students</a></h4></li>
              <ul>
                <li>Current Grad Students</li>
              </ul>
            <li><h4><a href="demos.html">Demos/Apps</a></h4></li>
              <ul>
                <li>City Creek</li>
                <li>Lake Waiau Photo Database</li>
                <li>Scientific Data App</li>
              </ul>
            <li><h4><a href="data.html">Data</a></h4></li>
              <ul>
                <li>Links &amp; Resources</li>
              </ul>
            <li><h4><a href="about.html">About</a></h4></li>
              <ul>
                <li>Contact</li>
              </ul>
          </ul>
      </div>-->
      </div><!-- /.row -->
    </div><!-- /.container -->


        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts.php"; ?>

  </body>
</html>
