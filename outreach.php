<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "includes/head.php"; ?>
</head>

<body>

  <?php include "includes/nav.php"; ?>

    <div class="jumbotron">
      <div class="container">
        <h1>Outreach</h1>
        <p></p>
      </div>
    </div>

    <div class="container content">

      <div class="row">
        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
          <h2>Videos</h2>
          <p>These videos were produced by the Spatial Data Analysis and Visualization Lab at the University of Hawaii in Hilo (UHH). I am an Affiliate Faculty member at UHH and former Supervisor of this Lab Facility. These videos highlight some of the research work that I initiated in Hawaii and continue to work on with collaborators.</p>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-6 col-md-6 col-lg-offset-3 col-md-offset-3">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe src="https://player.vimeo.com/video/46808252" class="embed-responsive-item" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-6 col-md-6 col-lg-offset-3 col-md-offset-3">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe src="https://www.youtube.com/embed/OYmXQeVAPXk"  class="embed-responsive-item" frameborder="0" allowfullscreen></iframe>
          </div>
        </div>
      </div>

    </div><!-- /.container -->


            <?php include "includes/footer.php"; ?>
            <?php include "includes/scripts.php"; ?>

  </body>
</html>
