<!DOCTYPE html>
<html lang="en">

<head>
<?php include "includes/head.php"; ?>
</head>
<!-- NAVBAR
================================================== -->

<body>
  <?php include "includes/nav.php"; ?>
      <div class="jumbotron">
          <div class="container" >
              <h1>Downloads</h1>
              <p></p>
          </div>
      </div>
<?php  ?>

    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container">
        <!-- Three columns of text below the carousel -->
	<div class="row">

	    <div class="col-md-4">
		<h3>Most Recent Version of Toolkit</h3>
		<a href="https://github.com/delparte/WCWAVE/archive/master.zip">Download Here</a>
	    </div>

            <div class="col-md-4">
                <h3>Reynolds Creek FTP</h3>
                <a href="ftp://ftp.nwrc.ars.usda.gov/publicdatabase/reynolds-creek/">ftp://ftp.nwrc.ars.usda.gov/publicdatabase/reynolds-creek/</a>
            </div>
	    
	    <!-- <div class="col-md-8">
		<h3>VWCSIT Poster</h3>
		<a href="../assets/other/vwcsit_poster.pdf"><img src="../assets/other/vwcsit_poster.jpg" class="img-responsive"></a>
	    </div>-->
	</div>
    </div>
    <!-- /.marketing -->


    <?php include "includes/footer.php"; ?>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <?php include "includes/scripts.php"; ?>

</body>

</html>
