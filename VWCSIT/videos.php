<!DOCTYPE html>
<html lang="en">

    <head>
        <?php include "includes/head.php"; ?>
    </head>
    <!-- NAVBAR
         ================================================== -->

    <body>
        <?php include "includes/nav.php"; ?>

        <div class="jumbotron">
            <div class="container" >
                <h1>Videos</h1>
                <p></p>
            </div>
        </div>

        <?php  ?>

        <!-- Marketing messaging and featurettes
             ================================================== -->
        <!-- Wrap the rest of the page in another container to center all the content. -->

        <div class="container marketing">
            <!-- Three columns of text below the carousel -->
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h2> Highcharts functionality </h2>
                </div>
                <div class="col-md-6 col-md-offset-3 text-center">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe src="https://www.youtube.com/embed/CmYZwXRCkjU" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h2> Using the Interpolation Tool </h2>
                </div>
                <div class="col-md-6 col-md-offset-3 text-center">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe src="https://www.youtube.com/embed/Dx8hH9GAglw" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h2> Using the WCWAVE Climate Station Interpolation Toolkit </h2>
                </div>
                <div class="col-md-6 col-md-offset-3 text-center">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe src="https://www.youtube.com/embed/KaizI0Af2bk" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h2> Using the WCWAVE CSIT Cross Validation Tools </h2>
                </div>
                <div class="col-md-6 col-md-offset-3 text-center">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe src="https://www.youtube.com/embed/lw4EM8M2obo" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.marketing -->


        <?php include "includes/footer.php"; ?>
        <!-- Bootstrap core JavaScript
             ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <?php include "includes/scripts.php"; ?>

    </body>

</html>
