<!DOCTYPE html>
<html lang="en">

<head>
<?php include "includes/head.php"; ?>
</head>
<!-- NAVBAR
================================================== -->

<body>
  <?php include "includes/nav.php"; ?>

      <div class="jumbotron">
          <div class="container" >
              <h1>Documentation</h1>
              <p></p>
          </div>
      </div>
<?php  ?>

    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

      <div class="container marketing">
          <div class="row">
              <div class="col-md-8 col-md-offset-2">
              <h1 id="wcwave-climate-station-interpolation-toolkit-csit-">WCWAVE Climate Station Interpolation Toolkit (CSIT)</h1>
              <p>CSIT is a set of tools for creating spatially distributed climate data that can be used to force climate models. You can find more information about these tools at <a href="http://geoviz.geology.isu.edu/delparte_labs/VWCSIT/index.php">the website</a></p>
              <h2 id="installation">Installation</h2>
              <p>This repository includes two toolboxes that can be used in an ArcMap for Desktop installation (requires 10.3+). Just download the repository and extract it in a place that can be connected to ArcMap.  There are three tools included in the toolboxes. </p>
              <ul>
                  <li>Gridding tools Toolbox<ol>
                      <li>Climate Data Gridding Tools</li>
                      <li>Reynolds Creek FTP to SQLite</li>
                  </ol>
                  </li>
                  <li>Cross Validation Toolkbox<ol>
                      <li>Cross Validation</li>
                  </ol>
                  </li>
              </ul>
              <p>The "Climate Data Gridding Tools" is the main tool used to create spatially distributed data. The "Reynolds Creek FTP to SQLite" tool can be used to convert <a href="ftp://ftp.nwrc.ars.usda.gov/publicdatabase/reynolds-creek/">Reynolds Creek FTP Data</a> into the SQLite format required for the tool. "Cross Validation" is the tool used to run leave-one-out cross validation for multiple timesteps and can create simple xy graphs to compare the accuracy of the interpolation process.</p>
              <p>The tool requires an elevation raster and a shapefile/featureclass of the station locations. To support Wind Speed the elevation raster needs to be in ASCII or geotiff format.  In some cases a soil station feature class with elevation in the attribute table. Optionally to support thermal radiation a View Factor Raster is required. View factor is calculated uses the techniques from  Dozier and Coutcalt (1979) equation 30 [Vf = cos^2(Horizon Angle)] (A horizon angle raster can be calculated with open source GIS tools. Such as GRASS GIS or Whitebox GIS).</p>
              <h3 id="server-installation">Server Installation</h3>
              <p><em>Check Back Later</em></p>
              <h3 id="description-of-files">Description of Files</h3>
              <ul>
                  <li>build_tables (dir) - old scripts for building the MySQL tables from ftp data</li>
                  <li>demo_data (dir) - Directory with sample data to test installations</li>
                  <li>joels_scripts (dir) - Origional scripts in separate files (used to create gridtools.py)</li>
                  <li>testing (dir) - directory used for testing scripts</li>
                  <li>utilites (dir) - ftp_2_sqlite.py script - used with Gridding Tools Toolbox</li>
                  <li>Cross_Validation.tbx - Cross validation toolbox for use in ArcMap</li>
                  <li>Gridding_Tools.tbx - Gridding Tools toolbox. Includes gridding tool and ftp2sqlite tool</li>
                  <li>cross_validation.py - script called from Cross Validation toolbox</li>
                  <li>gridtools.py - Gridding Tools script</li>
                  <li>gridtoolwrap.py - No longer supported. Used to create command line capable tool</li>
                  <li>runGriddingToolsJD.py - No longer supported. Gridding Tools on Johnston Draw (use gridtools.py now)</li>
              </ul>
              <h2 id="naming-convention">Naming convention</h2>
              <ul>
                  <li>initial:<ul>
                      <li>z: elevation (m)</li>
                      <li>z_0: roughness_length (m)</li>
                      <li>z_s: snow_depth (m)</li>
                      <li>rho: snow_density (kg m^-3)</li>
                      <li>T_s_0: active_snow_layer_temperature (&#176; C)</li>
                      <li>T_s: average_snow_covoer_temperature (&#176; C)</li>
                      <li>h2o_sat: H2O_saturation (%)</li>
                  </ul>
                  </li>
                  <li>precip:<ul>
                      <li>m_pp: precipitation_mass (kg m^-2 or mm)</li>
                      <li>%_snow: percent_snow (0 to 1.0)</li>
                      <li>rho_snow: precipitation_snow_density (kg m^-3)</li>
                      <li>T_pp: dew_point_temperature? (&#176;C)</li>
                  </ul>
                  </li>
                  <li>input:<ul>
                      <li>I_lw: thermal_radiation (W m^2)</li>
                      <li>T_a: air_temperature (&#176;C)</li>
                      <li>e_a: vapor_pressure (Pa)</li>
                      <li>u: wind_speed (m s^-1)</li>
                      <li>T_g: soil_temperature (&#176; C)</li>
                      <li>S_n: solar_radiation (W m^-2)</li>
                  </ul>
                  </li>
              </ul>
              <h2 id="references">References</h2>
              <p>Dozier, J., &amp; Outcalt, S. I. (1979). An Approach toward Energy Balance Simulation over Rugged Terrain. Geographical Analysis, 11(1), 74. Retrieved from <a href="http://onlinelibrary.wiley.com/doi/10.1111/j.1538-4632.1979.tb00673.x/pdf">http://onlinelibrary.wiley.com/doi/10.1111/j.1538-4632.1979.tb00673.x/pdf</a></p>
              </div>
          </div>
    </div>
    <!-- /.marketing -->


    <?php include "includes/footer.php"; ?>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <?php include "includes/scripts.php"; ?>

</body>

</html>
