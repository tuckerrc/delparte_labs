<a id="top"></a>

<div class="container-fullwidth header-nav">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-8 col-xs-6">
        <div class="header-logo-div">
          <a href="http://isu.edu"><img src="../assets/images/isu_logo_white.png" /></a>
        </div>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 pull-right">
        <div class="isu-div">
          <a href="http://geoviz.geology.isu.edu/delparte_labs/"><em>Delparte Labs</em></a><br>
          <img src="../assets/images/discover-foot.png" />
        </div>
      </div>
      <div class="col-md-8 col-xs-12">
        <div class="row">
          <div class="col-xs-10">
            <div class="project-title">
              <p>WC-WAVE Virtual Watershed</p>
              <p><em>&nbsp;Climate Station Interpolation Tools</em></p>
            </div>
          </div>
          <div class="col-xs-2">
            <a href="http://westernconsortium.org/"><img src="../assets/images/vwcsit/wcwave_logo.png" class="pull-right"></a>
          </div>
        </div>
        <hr></hr>
        <nav class="navbar navbar-inverse navbar-static-top">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                  <li id="index"><a href="index.php">Home</a></li>
                  <li id="interactive-map"><a href="interactive-map.php">Interactive Map</a></li>
                  <li id="dropdown">
                      <a href="#" class="dropdown-toggle"  data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Instructions <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                          <li id="videos"><a href="videos.php">Videos</a></li>
                          <li id="documentation"><a href="documentation.php">Documentation</a></li>
                      </ul>
                  </li>
                  <li id="downloads"><a href="downloads.php">Downloads</a></li>
                  <li id="how-to-cite"><a href="how-to-cite.php">How to Cite</a></li>
                  <li id="about" ><a href="about.php">About</a></li>
                  <li id="contact" ><a href="contact.php">Contact</a></li>
              </ul>
          </div>
        </nav>
      </div>

    </div>

  </div>
</div>
