<!-- FOOTER -->
<!-- FOOTER -->
<div class="container-fullwidth footer">
  <div class="container">
    <footer>
	<p><em><a href="http://westernconsortium.org/" style="color:white;">Project support through the Western Consortium for Watershed Analysis, Visualization and Exploration (WC-WAVE)</a><br>
National Science Foundation Award #IIA-1329513</em></p>
<p style="font-size: 11px"><em>Any opinions, findings, and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of the National Science Foundation.</em></p>

    </footer>
  </div>
</div>


<!-- 'Back to top' arrow that shows in the bottom right corner -->
<a href="#top" class="back-to-top">
 <span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
</a>
