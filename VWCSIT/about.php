<!DOCTYPE html>
<html lang="en">

    <head>
	<?php include "includes/head.php"; ?>
    </head>
    <!-- NAVBAR
	 ================================================== -->

    <body>
	<?php include "includes/nav.php"; ?>

	<div class="jumbotron">
            <div class="container" >
		<h1>About</h1>
		<p></p>
            </div>
	</div>
	<?php  ?>

	<!-- Marketing messaging and featurettes
	     ================================================== -->
	<!-- Wrap the rest of the page in another container to center all the content. -->

	<div class="container marketing">
            <!-- Three columns of text below the carousel -->

            <!-- <div class="row">
		 <div class="col-xs-12">
		 <p>This project is part of the WC-WAVE initiative. (<a href="http://westernconsortium.org/">http://westernconsortium.org/</a>)
		 </div> 

		 </div>--> <!-- /.row -->
            <div class="row">
		<div class="col-xs-8 col-xs-offset-2 text-center">
		    <h3>The Team</h3>
		    <hr>

		</div>

            </div> <!-- /.row -->

            <div class="row">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center animation-element slide-up">
		    <div class="team-image  animation-element scale">
			<img src="..\assets\images\delparte.jpg" />
		    </div>
		    <br>
		    <div class="team-info">
			Dr. Donna Delparte, <br>Assistant Professor, Geosciences, <br> Idaho State University
		    </div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center animation-element slide-up">
		    <div class="team-image animation-element scale">
			<img src="..\assets\images\chapman.jpg" />
		    </div>
		    <br>
		    <div class="team-info">
			ISU Masters of Geographic Information Sciences <br>Graduate Research Assistant: Tucker Chapman
		    </div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center animation-element slide-up">
		    <div class="team-image animation-element scale">
			<img src="..\assets\images\johansen.jpg" />
		    </div>
		    <br>
		    <div class="team-info">
			ISU Masters of Geographic Information Sciences<br> Graduate Research Assistant: Joel Johansen
		    </div>
		</div>
            </div> <!-- /.row  -->
	    <div class="row">
		<div class="col-xs-8 col-xs-offset-2 text-center">
		    <h3>Presentations</h3>
		    <hr>

		</div>
	    </div> <!-- /.row -->

	    <div class="row">
		<div class="col-md-8 col-md-offset-2 animation-element slide-up">
		    <h3>VWCSIT Poster</h3>
		    <a href="../assets/other/vwcsit_poster.pdf" target="_blank"><img src="../assets/other/vwcsit_poster.jpg" class="img-responsive"></a>
		</div>
	    </div>
	</div>
	<!-- /.marketing -->


	<?php include "includes/footer.php"; ?>
	<!-- Bootstrap core JavaScript
	     ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<?php include "includes/scripts.php"; ?>

    </body>

</html>
