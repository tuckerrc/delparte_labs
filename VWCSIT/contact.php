<!DOCTYPE html>
<html lang="en">

<head>
<?php include "includes/head.php"; ?>
</head>
<!-- NAVBAR
================================================== -->

<body>
  <?php include "includes/nav.php"; ?>

      
      <div class="jumbotron">
          <div class="container" >
              <h1>Contact</h1>
              <p></p>
          </div>
      </div>

<?php  ?>

    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container-fluid marketing">
        <!-- Three columns of text below the carousel -->
      <div class="container">

        For more information please contact: <br><br>
        <strong>Dr. Donna Delparte</strong><br>
        <strong>Assistant Professor</strong><br>
        Department of Geosciences<br>
        Idaho State University<br>
        Pocatello, ID 83209, USA<br>
        <a href="mailto:delparte@isu.edu">delparte@isu.edu</a><br>

      </div><!-- /.container -->

    </div>
    <!-- /.marketing -->


    <?php include "includes/footer.php"; ?>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <?php include "includes/scripts.php"; ?>

</body>

</html>
