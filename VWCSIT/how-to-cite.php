<!DOCTYPE html>
<html lang="en">

<head>
<?php include "includes/head.php"; ?>
</head>
<!-- NAVBAR
================================================== -->

<body>
  <?php include "includes/nav.php"; ?>
      <div class="jumbotron">
          <div class="container" >
              <h1>Cite</h1>
              <p></p>
          </div>
      </div>

<?php  ?>

    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">
        <!-- Three columns of text below the carousel -->
        <div class="row">
          <div class="col-xs-12">
            <h3>How do I cite material from this website?</h3>
          </div>
        </div>
    </div>
    <!-- /.marketing -->


    <?php include "includes/footer.php"; ?>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <?php include "includes/scripts.php"; ?>

</body>

</html>
