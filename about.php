<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include "includes/head.php"; ?>
  </head>

  <body>

    <?php include "includes/nav.php"; ?>

    <div class="jumbotron">
      <div class="container">
        <h1>About</h1>
      <p></p>
    </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-lg-8 col-md-8">

          <img src="assets/images/delparte_headshot.jpg" alt="Dr. Donna Delparte" width="150" height="184" hspace="50" border="0" align="right">
          <h2>Position</h2>
          Assistant Professor, Department of Geosciences
          <h2>Education</h2>
          PhD. - University of Calgary, Canada<br>
          MSc. - University of Calgary, Canada<br>
          BSc. - University of Regina, Canada<br>
          <h2>Research</h2>
          Dr. Delparte has an extensive background in the applications of GIS and remote sensing to the fields of geosciences, resource management and conservation/environmental planning. Dr. Delparte's current research focus relates to visualization, 3D modeling and analysis. She is using 3D platforms to visualize her research work with photogrammetry, Structure from Motion (SfM), LiDAR and point-cloud generation from gaming devices. Specific research applications relate to avalanche flow modeling and hazard mapping, terrain models, land cover change, precision agriculture and image analysis.Her professional experience also extends to government and industry sectors.
          <h2>Selected Publications</h2>
          <ul>
          <li>Delparte, D., Peterson, M., Perkins, J. and Jackson, J. 2013. Integrating Gaming Technology to Map Avalanche Hazard. 2013 International Snow Science Workshop, Chamonix Mont-Blanc.</li>
          <li>Delparte, D., M. Peterson, J. Jackson and J. Perkins. 2012. Modeling and visualizing avalanche flow using genetic algorithms and OpenGL. International Snow Science Workshop 2012. Anchorage, AK.</li>
          <li>VanZandt, M., Delparte, D., Duvall, D., Penniman, J., 2013. Nesting Distribution and Habitat Selection of the endangered Hawaiian Petrel (Pterodroma sandwichensis) on the Island of Lanāi. Waterbirds.  (Accepted).</li>
          <li>Wu, J., Delparte, D., Hart, P. 2014. Movement patterns of a native and non-native frugivore in Hawai‘i and implications for seed dispersal. Biological Conservation. (Accepted).</li>
          <li>Melrose, J. and D. Delparte. 2012. Hawaii County Food Self-Sufficiency Baseline. County of Hawaii Research and Development Department. 212pp.</li>
          <li>Giambelluca, T., Q. Chen, A. Frazier, J. Price, Y. Chen, P. Chu, J. Eischeid, D. Delparte. 2013. Online Rainfall Atlas of Hawaii. Bulletin of the American Meteorological Society. 94, 313–316 (http://dx.doi.org/10.1175/BAMS-D-11-00228.1)</li>
          <li>Delparte, D. 2011. Small-Scale Geospatial Data Repositories: If You Build It, Will They Come? Position Paper and Presentation for the First NSF CyberGIS Project All Hands Meeting, September 28-30, Oak Ridge National Laboratory, Tennessee</li>
          <li>Delparte, D., Jamieson, B. and Waters, N., 2008. Statistical runout modeling of snow avalanches in Glacier National Park, Canada. Cold Regions Science and Technology. 54, pp.183-192.</li>
          <li>Delparte, D. 2008. Avalanche Terrain Modeling in Glacier National Park, Canada. PhD Thesis. Department of Geography. University of Calgary, Calgary, AB, Canada, p 179</li>
          <li>Delparte, D. M. 2006. The Use of GIS in Avalanche Modeling. Knowledge Media Technologies, First International Core-to-Core Workshop. No. 21, Dagstuhl, Germany.</li>
          <li>D'Eon, R.G. and Delparte, D. M. 2005. Effects of radiocollar position and orientation on GPS-radiocollar performance, and the implications of PDOP in data screening. Journal of Applied Ecology. 42(2), pp. 383-388</li>
        </div>
        <div class="col-lg-4 col-md-4">
          <h3>Donna Delparte, PhD</h3>
          <p>Assistant Professor,<br>
          Department of Geosciences<br>
          Idaho State University<br>
          921 S 8th Ave, STOP 8072<br>
          Pocatello, ID 83209-8072<br>
          Phone: 208-282-4419<br>
          Fax: 208-282-4414<br>
          <a href="mailto:delparte@isu.edu">Email</a></p>
      </div>
    </div><!-- /.row -->

    </div><!-- /.container -->

    <?php include "includes/footer.php"; ?>
    <?php include "includes/scripts.php"; ?>


  </body>
</html>
