<!DOCTYPE html>
<html lang="en">
  <head>
    <head>
      <?php include "includes/head.php"; ?>
    </head>

    <body>

      <?php include "includes/nav.php"; ?>

      <div class="jumbotron">
        <div class="container">
          <h1>Data</h1>
        </div>
      </div>

    <div class="container content">
      <div class="row">
        <div class="col-lg-10 col-md-10 col-lg-offset-1 col-md-offset-1">
          <p>Finding the datasets you need for your GIS or remote sensing projects can be a challenging task. Here are a few of my favorite links for accessing free data sources:</p>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4 col-lg-offset-3 col-md-offset-3 col-sm-offset-2">
            <h3>National Data Sources:</h3>
            <ul>
              <li><a href="http://earthexplorer.usgs.gov/">Earth Explorer</a></li>
              <li><a href="http://nationalmap.gov/viewer.html">USGS National Map</a></li>
              <li><a href="http://www.ngdc.noaa.gov/mgg/bathymetry/relief.html">NOAA Bathymetry and DEM Data</a></li>
              <li><a href="http://www.data.gov/">Data.gov</a></li>
              <li><a href="http://www2.census.gov/">Census (tabular data)</a></li>
            </ul>

        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <h3>Idaho Specific:</h3>
            <ul>
              <li><a href="http://inside.uidaho.edu/">Inside Idaho</a></li>
              <li><a href="http://www.idaholidar.org/">Idaho LiDAR</a></li>
              <li><a href="http://giscenter.isu.edu/data/index.htm">GIS Trec</a></li>

            </ul>

        </div>
      </div>
    </div><!-- /.container -->

    <?php include "includes/footer.php"; ?>
    <?php include "includes/scripts.php"; ?>


  </body>
</html>
